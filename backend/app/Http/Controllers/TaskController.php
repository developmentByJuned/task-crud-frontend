<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Task;

use Validator;

class TaskController extends Controller
{
    public function index ( Request $request ) {
        $user_id = $request->input('user_id');
        if( ! $user_id) {
            return response()->json([
                'status'=> 500,
                'message' => 'user id is required'
            ]);    
        }
        $tasks = Task::all()->whereIn( 'user_id', $user_id );
        return response()->json([
            'status'=> 200,
            'data' => $tasks
        ]);
    }

    public function store ( Request $request) {
        $validator = $this->validateTask();
        if($validator->fails()){
            return response()->json([ 'status' => '500', 'message' => $validator->messages()]);
        }

        $task = Task::create([
            'title' => $request->get('title'),
            'message' => $request->get('message'),
            'status' => false,
            'user_id' => $request->get('user_id')
        ]);
        return response()->json([
            'status' => '201',
            'data' => $task,
            'message' => 'Task Created Successfully'
        ]);
    }

    public function show ( $id ) {
        $task = Task::find($id);
        
        if( ! $task ) {
            return response()->json([
                'status' => '404',
                'message' => 'Task Not Found'
            ]);
        }
        
        return response()->json([
            'status' => '200',
            'data' => $task
        ]);   
    }

    public function update ( Request $request, $id ) {
        $task = Task::find($id);
        if( ! $task ) {
            return response()->json([
                'status' => '404',
                'message' => 'Task Not Found'
            ]);
        }

        $task->title = $request->input('title', $task->title );
        $task->message = $request->input('message', $task->message );
        $task->status = $request->input('status', $task->status );
        $task->save();

        return response()->json([
            'status' => '200',
            'data' => $task,
            'message' => 'Update Successful'
        ]);
    }

    public function delete ( $id ) {
        $task = Task::find($id);
        if( ! $task ) {
            return response()->json([
                'status' => '404',
                'message' => 'Task Not Found'
            ]);
        }
        $task->delete();
        return response()->json([
            'status' => '200',
            'message' => 'Task Deleted Successfully'
        ]);
    }

    public function validateTask(){
        return Validator::make( request()->all(), [
            'title' => 'required|string|max:50',
            'message' => 'required|string|max:500',
            'user_id' => 'required|exists:users,id'
        ]);
    }
}
