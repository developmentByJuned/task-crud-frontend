import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Login from './containers/Login';
import Home from './containers/Home';
import Signup from './containers/Signup';

import './App.css';
function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/home" exact component={ Home } />
          <Route path="/signup" exact component={ Signup } />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
