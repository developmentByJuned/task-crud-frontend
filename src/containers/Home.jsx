import React , { useState , useEffect }from 'react';
import '../styles/home.scss';
import { Card , Button , Form} from 'react-bootstrap';
import Task from '../components/Task';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import TaskModal from '../components/TaskModal';
import TaskForm from '../components/TaskForm';
import Request from './../api/index';

const Home = ( props ) => {
    const [ isOpen, setIsOpen ] = useState(false);
    const [ taskList, setTaskList ] = useState({});
    const [ loading, setLoading ] = useState(true);
    const [ selectedTask, setSelectedTask ] = useState({});
    
    useEffect(() => {
        const user_id = sessionStorage.getItem('session_user_id');
        const tasks = Request('/tasks?user_id='+ user_id, 'GET' );
        tasks.then( ({ data }) => {
            let finalData = [];
            for (var key of Object.keys( data )) {
                finalData.push(data[key]);
            }
            console.log(finalData)
            setTaskList({...finalData})
            setLoading(false);
        });
    }, []);

    const showTask = ( task ) => {
        var activeItem = document.querySelector(".custom-active");
        if(activeItem !==null){
            activeItem.classList.remove("custom-active");
        }
        document.getElementById(task.id).classList.add('custom-active');
        setSelectedTask({...task});
    }
    
    return (
        <>
        <div className="home-container">
            <TaskModal open = { isOpen } modalTitle="Add Task Details" onClose = { () => setIsOpen(false)} >
                <TaskForm onClose = { () => setIsOpen(false)} />
            </TaskModal>
            <div className="container p-5 ml-5">
                <div className="row">
                    <div className="col-4">
                        <Card className="custom-card">
                            <Card.Body style={{ textAlign : "center", maxHeight: "80vh", overflowY:"auto" }} >
                                
                                <div className="heading" style={{ marginBottom: "10%"}}> All tasks </div>
                                { loading ? 
                                    <h4>Loading...</h4> :
                                    <ul className="task-list p-0">
                                    { taskList && Object.values(taskList).map( task => {
                                        return(<li id={task.id} onClick={ ()=> showTask( {...task})  } key={task.id} >{ task.title }</li>)
                                    })}
                                </ul>
                                }
                                
                            </Card.Body>
                            <Card.Footer>
                                    <Button block bsSize="large" onClick={ () => setIsOpen(true) } variant="success">
                                        <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                                        &nbsp;&nbsp; Add Task
                                    </Button>{' '}
                            </Card.Footer>
                                
                        </Card>
                    </div>

                    <div className="col-8">
                        <Card className="custom-card">
                            <Card.Body>
                                <div className="heading">Task Manager</div>
                                <Task task={selectedTask}></Task>
                            </Card.Body>
                        </Card>
                    </div>
                </div>
            </div>
       </div>
        </>
    );
}

export default Home;