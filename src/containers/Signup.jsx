import React, { useState } from 'react';
import '../styles/login.scss';
import { Button ,FormGroup, FormControl, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope , faUser, faKey} from '@fortawesome/free-solid-svg-icons'
import { useHistory, Link } from 'react-router-dom';
import Request from './../api/index';

const Signup = (props) => {
    const [ name, setName ] = useState("");
    const [ email, setEmail ] = useState(""); 
    const [ password, setPassword ] = useState("");
    const history = useHistory();

    const handleSubmit = async ( event ) => {
        event.preventDefault();
        validateForm();
        const userData = {
            name,
            email,
            password
        };
        const response = await Request('/signup', 'POST', userData );
        if( response.status === '201' ) {
            // redirect to login
            history.push("/")
        }
        
    }

    const validateForm = () => {
        if( (name.length === 0) && (email.length === 0) && (password.length === 0) ) {
            return false;
        }
        return true;
    }

    return (
    <>
     <div className="login-container">
        <div className="Login">
            <Form onSubmit={handleSubmit}>
                <h1 className="heading">Signup</h1>
                <FormGroup controlId="email" bsSize="large">
                    <Form.Label> 
                        <FontAwesomeIcon icon={faUser} />
                        &nbsp;Name
                    </Form.Label>
                    <FormControl
                        autoFocus
                        type="text"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                </FormGroup>
                <FormGroup controlId="email" bsSize="large">
                    <Form.Label> 
                        <FontAwesomeIcon icon={faEnvelope} />
                        &nbsp;Email address
                    </Form.Label>
                    <FormControl
                        autoFocus
                        type="email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </FormGroup>
                <FormGroup controlId="password" bsSize="large">
                    <Form.Label>
                        <FontAwesomeIcon icon={faKey} />
                        &nbsp;Password
                    </Form.Label>
                    <FormControl
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type="password"
                    />
                </FormGroup>
                <Button className="custom-button" block bsSize="large" disabled={!validateForm()} type="submit">
                Sign Up
                </Button>
                <label className="signup-link" >Already registered ! <strong><u> <Link to="/">Login Now</Link></u> </strong></label>
            </Form>
           
        </div>
        </div> 
    </>
    );
}
 
export default Signup;