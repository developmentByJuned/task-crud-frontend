<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function login ( Request $request ) {
        $validator = $this->validateEmail();
        if($validator->fails()){
            return response()->json([ 'status' => '500', 'message' => $validator->messages()]);
        }
        $user = User::whereEmail($request->email)->first();

        if( ! $user ){
            return response()->json([ 'status' => 404, 'message' => 'Email Wrong' ]);  
        }

        if(Hash::check($request->password, $user->password)){
            return response()->json([
                'status'=> 200,
                'data' => array( 'name' => $user->name, 'email'=> $user->email , 'user_id'=>$user->id ),
                'message' => 'Login Successful']);
        }
            return response()->json([ 'message' => 'Incorrect Password', 'status' => 401]);
    }

    public function signup ( Request $request ) {
        $validator = $this->validateUser();
        if($validator->fails()){
            return response()->json([ 'status' => '500', 'message' => $validator->messages()]);
        }
        $userExist = User::whereEmail($request->email)->first();
        if( $userExist ) {
            return response()->json([
                'status'=>"401",
                'message'=>"User with this Email Already Exists"
            ]);
        }
        $user = User::create([
            'name' => $request->get('name'),
            'password' => Hash::make($request->get('password')),
            'email' => $request->get('email')
        ]);

        return response()->json([
            'status' => '201',
            'data' => array(
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at,
            ),
            'message' => 'User Created Successfully'
        ]);
    }

    public function validateUser(){
        return Validator::make( request()->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|max:30'
        ]);
    }
    public function validateEmail(){
        return Validator::make( request()->all(), [
            'email' => 'required|string|email|max:255'
        ]);
    }
}
