import React, { useState, useRef } from 'react';
import { Form , Button, Alert } from 'react-bootstrap';
import Request from './../api/index';
const TaskForm = ( props ) => {

    const submitRef = useRef();
    const [ title, setTitle ] = useState('');
    const [ message, setMessage ] = useState('');
    const [ responseMessage , setResponseMessage ] = useState('');
    const [ show , setShow ] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        if(submitRef.current){
            submitRef.current.setAttribute("disabled", "disabled");
        }
        const taskData = {
            title,
            message,
            user_id: sessionStorage.getItem('session_user_id')
        };
       
        const submitData = await Request('/tasks', 'POST', taskData);

        if( submitData.status === '201' ) {
            setResponseMessage(submitData.message);
            setShow('success');
            setTimeout( ()=> {
               window.location.reload();
            },200);
        }else{
            setResponseMessage('Something went wrong !!');
            setShow('failed');
        }
        
    }

    return (
        <>
        <Form onSubmit= { handleSubmit.bind()}>
            <Form.Group controlId="forTitle">
                <Form.Label> <strong> Title </strong></Form.Label>
                <Form.Control onChange={ (event) => setTitle(event.target.value)} type="text" placeholder="Enter Title of the Task" />
                
            </Form.Group>

            <Form.Group controlId="forMessage">
                <Form.Label><strong>Message</strong></Form.Label>
                <Form.Control onChange={ (event) => setMessage(event.target.value)} as="textarea" rows={3} placeholder="Please Enter the Message" />
               
            </Form.Group>

            <Button variant="success" type="submit" ref={submitRef} >
                Save Task
            </Button>
        </Form>
        <div className="mt-4">
            { show === 'failed' ?
                <Alert variant="danger" onClose={() => setShow('')} dismissible>
                    <Alert.Heading>{ responseMessage }</Alert.Heading>
                </Alert>
            : null
            }
            { show === 'success' ?
                <Alert variant="success" onClose={() => setShow('')} dismissible>
                    <Alert.Heading>{ responseMessage }</Alert.Heading>
                </Alert>
            : null
            }
        </div>
        </>
    );
} 

export default TaskForm;