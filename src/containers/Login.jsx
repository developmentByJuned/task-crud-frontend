import React, { useState } from 'react';
import '../styles/login.scss';
import { Button ,FormGroup, FormControl, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope , faKey} from '@fortawesome/free-solid-svg-icons'
import { useHistory , Link} from 'react-router-dom';
import Resource from './../api/index';

const Login = (props) => {
    const [ email, setEmail ] = useState(""); 
    const [ password, setPassword ] = useState("");
    const history = useHistory();

    const handleSubmit = async ( event ) => {
        event.preventDefault();
        // redirect to home
        const loginCreds = {
            email,
            password
        };

        const response = await Resource('/login', 'POST', loginCreds );
        if( response.status === 200) {
            sessionStorage.setItem('session_user_id', response.data.user_id );
            history.push("/home")
        }
        
    }
    const validateForm = () => {
        return true;
    }
    return (
    <>
     <div className="login-container">
        <div className="Login">
            <Form onSubmit={handleSubmit}>
                <h1 className="heading">Login</h1>
                <FormGroup controlId="email" bsSize="large">
                    <Form.Label> 
                        <FontAwesomeIcon icon={faEnvelope} />
                        &nbsp;Email address
                    </Form.Label>
                    <FormControl
                        autoFocus
                        type="email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </FormGroup>
                <FormGroup controlId="password" bsSize="large">
                    <Form.Label>
                        <FontAwesomeIcon icon={faKey} />
                        &nbsp;Password
                    </Form.Label>
                    <FormControl
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type="password"
                    />
                </FormGroup>
                <Button className="custom-button" block bsSize="large" disabled={!validateForm()} type="submit">
                Login
                </Button>
                <label className="signup-link" >Not registered yet ! <strong><u> <Link to="/signup">Register Now</Link></u> </strong></label>
            </Form>
           
        </div>
        </div> 
    </>
    );
}
 
export default Login;