import React from 'react';
import { Modal } from 'react-bootstrap';

const TaskModal = ({ open, onClose, modalTitle, children }) => {
    
    if( !open ) return null;

    return (
        <div>
          <Modal show={open} onHide={onClose}>

            <Modal.Header closeButton>
              <Modal.Title> { modalTitle } </Modal.Title>
            </Modal.Header>

            <Modal.Body>{ children }</Modal.Body>

          </Modal>
        </div>
    )
}

export default TaskModal;