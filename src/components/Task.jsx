import React, { useState , useEffect, useRef } from 'react';
import '../styles/task.scss';
import { Button, Alert } from 'react-bootstrap';
import Request from './../api/index';

const Task = ( { task } ) => {

    const doneRef = useRef();
    const deleteRef = useRef();

    const [ show, setShow ] = useState('');
    const [ taskState, setTaskState ] = useState(task);
    useEffect(() => {
        if (task) {
            setTaskState(task);
        }
      }, [task])

    if( Object.keys(task).length === 0 && task.constructor === Object ) {
        return (
            <h4>No task selected !!!</h4>
        )
    }

    const handleStatusChange = async () => {
        const updateData = {
            status: true
        };

        if(doneRef.current){
            doneRef.current.setAttribute("disabled", "disabled");
        }

        const changeStatus = await Request('/tasks/'+task.id , 'PUT' , updateData );
        if( changeStatus.status === '200') {
            setShow('success');
            const fetchTask = await Request('/tasks/' + taskState.id , 'GET');
            setTaskState(fetchTask.data);
            setTimeout(()=>{
                setShow('');
                window.location.reload();
            },500)
        }else{
            setShow('failed');
            setTimeout(()=>{
                setShow('');
            },500)
        }
    }

    const handleDeleteTask = async () => {
        if(deleteRef.current){
            deleteRef.current.setAttribute("disabled", "disabled");
          }
        const DeleteTask = await Request('/tasks/'+ taskState.id , 'DELETE');
        if( DeleteTask.status === '200') {
            setShow('delete');
            window.location.reload();
        } else {
            setShow('failed');
            setTimeout(()=>{
                setShow('');
            },500)
        }
    }

    return (
        <>
        <h4>Task Details</h4>
        <ul>
            <li>
                <div className="row">
                    <div className="col-4"><label><strong>Title</strong></label></div>
                    <div className="col-1">:</div>
                    <div className="col-7"> {taskState.title}</div>
                </div>
            </li>
            <li>
                <div className="row">
                    <div className="col-4"><label><strong>Message</strong></label></div>
                    <div className="col-1">:</div>
                    <div className="col-7"> { taskState.message }</div>
                </div>
            </li>
            <li>
                <div className="row">
                    <div className="col-4"><label><strong>Created At</strong></label></div>
                    <div className="col-1">:</div>
                    <div className="col-7"> { new Date(taskState.created_at).toDateString() }</div>
                </div>
            </li>
            { taskState.status ?
            <li>
                <div className="row">
                    <div className="col-4"><label><strong>Completion Date</strong></label></div>
                    <div className="col-1">:</div>
                    <div className="col-7"> { new Date(taskState.updated_at).toDateString() }</div>
                </div>
            </li>
            : null
            }
            <li>
                <div className="row">
                    <div className="col-4"><label><strong>Status</strong></label></div>
                    <div className="col-1">:</div>
                    <div className="col-7"> { taskState.status ? 'Done' : 'Pending' }</div>
                </div>
            </li>
        </ul>
        <div className="row mt-5">
            { ! taskState.status ?
                <div className="col-2">  
                    <Button block bsSize="large" ref={doneRef} variant="primary" onClick={handleStatusChange.bind()}>Done</Button>{' '}
                </div>
                : null
            }
                <div className="col-2">  
                    
                    <Button block bsSize="large" ref={deleteRef} variant="danger" onClick={handleDeleteTask.bind()}>Delete</Button>{' '}
                </div>
        </div>
        <div className="custom-alerts mt-4">
        {   show === 'success' ?
            <Alert variant="success" onClose={ () => setShow('') } dismissible>
                <Alert.Heading>Task Marked Completed !!!</Alert.Heading>
            </Alert>
            : null
        }
        {   show === 'delete' ?
            <Alert variant="success" onClose={ () => setShow('') } dismissible>
                <Alert.Heading>Task Deleted Successully !!!</Alert.Heading>
            </Alert>
            : null
        }
        {   show === 'failed' ?
            <Alert variant="danger" onClose={ () => setShow('') } dismissible>
                <Alert.Heading>Something went wrong ... Updation failed !!!</Alert.Heading>
            </Alert>
            : null
        }
        </div>
        </>
    )
}

export default Task ;