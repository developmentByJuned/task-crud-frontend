import axios from 'axios';
import Cookies from 'js-cookie';

const API_HOSTNAME = 'http://127.0.0.1:8000/api';

const Request = (endpoint = '', method = 'GET', data = null, params = null) =>
  new Promise((resolve, reject) => {
    try {
      const headers = {};
      const token = Cookies.get('token') || null;
      if (token && token !== 'undefined') {
        headers.Authorization = token && `Bearer ${token}`;
      }
      return axios({
        url: API_HOSTNAME + endpoint,
        method,
        data,
        params,
        headers
      })
        .then((response) => {
          // console.log(response);
          resolve(response.data);
        })
        .catch((err) => {
          console.log(err);
          resolve(err.response && err.response.data);
        });
    } catch (e) {
      reject(e);
    }
  });

export default Request;
