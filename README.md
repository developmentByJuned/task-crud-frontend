# task-crud-frontend

## backend setup steps

* goto root repo folder then
* cd backend ( go to backend folder in cmd or terminal )
* change .env.example.com with .env
* composer install
* php artisan key:generate
* php artisan serve

## frontend setup steps

* goto root folder
* run command npm i
* npm run start

## notice

* setup db creds in .env file under backedn
* if your backend is running in different port. Update the url into API folder under index file. Change API_HOSTNAME
* make sure backend is running while using frontend.